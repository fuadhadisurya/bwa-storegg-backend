const mongoose = require('mongoose');
let transactionSchema = mongoose.Schema({
    historyVoucherTopup: {
        gameName: { type: String, require: [true, "Nama game harus diisi"] },
        category: { type: String, require: [true, "Kategori harus diisi"] },
        thumbnail: { type: String },
        coinName: { type: String, require: [true, "Nama koin harus diisi"] },
        coinQuantity: { type: Number, require: [true, "Jumlah koin harus diisi"] },
        price: { type: Number },
    },
    historyPayment: {
        name: { type: String, require: [true, "Nama harus diisi"] },
        type: { type: String, require: [true, "Tipe pembayaran harus diisi"] },
        bankName: { type: String, require: [true, "Nama bank harus diisi"] },
        noRekening: { type: String, require: [true, "Nomor Rekening harus diisi"] }
    },
    name: {
        type: String,
        require: [true, 'Nama harus diiisi'],
        maxlength: [225, "Panjang nama harus antara 3-255 Karakter"],
        minlength: [3, "Panjang nama harus antara 3-255 Karakter"],
    },
    accountUser: {
        type: String,
        require: [true, 'Nama akun harus diiisi'],
        maxlength: [225, "Panjang nama harus antara 3-255 Karakter"],
        minlength: [3, "Panjang nama harus antara 3-255 Karakter"],
    },
    tax: {
        type: Number,
        default: 0
    },
    value: {
        type: Number,
        default: 0
    },
    status: {
        type: String,
        enum: ['pending', 'success', 'failed'],
        default: 'pending'
    },
    player: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Player'
    },
    historyUser: {
        name: { type: String, require: [true, "Nama player harus diisi"] },
        phoneNumber: { 
            type: String,
            require: [true, 'Nomor Telepon harus diiisi'],
            maxlength: [13, "Panjang nama harus antara 9-13 Karakter"],
            minlength: [9, "Panjang nama harus antara 9-13 Karakter"]
        },
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
}, { timestamps: true })

module.exports = mongoose.model('Transaction', transactionSchema)
