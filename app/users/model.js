const mongoose = require('mongoose');
let userSchema = mongoose.Schema({
    email: {
        type: String,
        require: [true, 'Email harus diiisi']
    },
    name: {
        type: String,
        require: [true, 'Nama harus diiisi']
    },
    password: {
        type: String,
        require: [true, 'Kata sandi harus diiisi']
    },
    status: {
        type: String,
        enum: ['admin', 'user'],
        default: 'user'
    },
    status: {
        type: String,
        enum: ['Y', 'N'],
        default: 'Y'
    },
    phoneNumber: {
        type: String,
        require: [true, 'Nomor Telepon harus diisi'],
    },
}, { timestamps: true })

module.exports = mongoose.model('User', userSchema)
